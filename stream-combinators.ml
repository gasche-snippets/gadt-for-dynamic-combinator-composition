(* a small code snippet to answer the following question by whitequark on IRC: 

   < whitequark> I want to be able to describe data flow between
   components. Each component could be a function, (a Stream.t *
   b Stream.t * ...) -> (c Stream.t * ...), where a, b, c are some
   concrete types specific for the component

   < whitequark> I want to describe it externally to the OCaml program,
   in, say, JSON.

   < whitequark> is there a way of safely instantiating such components
   from such a description without packing all stream
   elements into one huge type t = A of a | B of b |
   ... and then dynamically checking it on the
   boundaries?
*)

module Example (M: sig
  module Stream : sig type 'a t end

  (* we have to do this wrapping in an explicit datatype for technical
     reasons linked to abstract type (non-)injectivity *)
  type 'a stream = S of 'a Stream.t

  val generate : unit -> int stream
  val split : int stream -> (int stream * int stream)
  val square : int stream -> int stream
  val display : int stream -> unit
end) = struct
  open M

  type _ typ =
  | Unit : unit typ
  | Int : int typ
  | Stream : 'a typ -> 'a stream typ
  | Pair : 'a typ * 'b typ -> ('a * 'b) typ
  | Fun : 'a typ * 'b typ -> ('a -> 'b) typ
    
  type 'a typed = 'a typ * 'a

  let generate : _ typed = (Fun (Unit, Stream Int), generate)
  let split : _ typed = (Fun (Stream Int, Pair (Stream Int, Stream Int)), split)
  let square : _ typed = (Fun (Stream Int, Stream Int), square)
  let display : _ typed = (Fun (Stream Int, Unit), display)

  type dynamic = Dyn : 'a typed -> dynamic

  let database = [
    "generate", Dyn generate;
    "split", Dyn split;
    "square", Dyn square;
    "display", Dyn display;
  ]

  (* the (untyped) stuff that is returned from dynamically parsing
     your configuration file *)
  type plan =
  | Op of string
  | Var of string
  | Unit
  | Const of int
  | Let of string * plan * plan
  | LetP of (string * string) * plan * plan
  | Apply of plan * plan
  | Pair of plan * plan

(* (*plan example:*)
  let s1 = generate ()
  let (s2, s3) = split s1
  let s2' = square s2
  let (), () = display s2', display s3
*)

  let plan =
    Let ("s1", Apply (Op "generate", Unit),
    LetP (("s2", "s3"), Apply(Op "split", Var "s1"),
    Let ("s2'", Apply(Op "square", Var "s2"),
    Pair (Apply (Op "display", Var "s2'"), Apply (Op "display", Var "s3"))
    )))

  let err_type_expected _ _ =
    (* todo user-friendly message *)
    failwith "error (type expected)"

  type (_, _) typ_eq =
  | Eq : ('a, 'a) typ_eq

  let rec check_eq : type a b . a typ * b typ -> (a, b) typ_eq = function
    | Int, Int -> Eq
    | Unit, Unit -> Eq
    | Stream ty1, Stream ty2 ->
      let Eq = check_eq (ty1, ty2) in Eq
    | Pair (ta1, tb1), Pair (ta2, tb2) ->
      let Eq = check_eq (ta1, ta2) in
      let Eq = check_eq (tb1, tb2) in
      Eq
    | Fun (ta1, tb1), Fun (ta2, tb2) ->
      let Eq = check_eq (ta1, ta2) in
      let Eq = check_eq (tb1, tb2) in
      Eq
    | ty1, ty2 ->
      (* todo user-friendly type printing; note that you get the
         outermost distinct sub-types for free*)
      failwith "error (incompatible types)"

  let rec run env = function
    | Op str -> List.assoc str database
    | Var x -> List.assoc x env
    | Unit -> Dyn (Unit, ())
    | Const n -> Dyn (Int, n)
    | Let (x, e1, e2) -> run ((x, run env e1) :: env) e2
    | Pair (e1, e2) ->
      let (Dyn (ty1, v1), Dyn (ty2, v2)) = run env e1, run env e2 in
      Dyn (Pair (ty1, ty2), (v1, v2))
    | LetP ((x1, x2), e1, e2) ->
      begin match run env e1 with
        | Dyn (Pair (ty1, ty2), (v1, v2)) -> 
          let d1, d2 = Dyn (ty1, v1), Dyn (ty2, v2) in
          run ((x1, d1) :: (x2, d2) :: env) e2
        | Dyn (errty, _) -> err_type_expected "pair" errty
      end
    | Apply (e1, e2) ->
      let (Dyn (funty, f)) = run env e1 in
      let (Dyn (argty, v)) = run env e2 in
      begin match funty with
        | Fun (argty', resty) ->
          let Eq = check_eq (argty, argty') in Dyn (resty, f v)
        | errty -> err_type_expected "function" errty
      end
end    
